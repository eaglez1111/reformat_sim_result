import csv
import os
import numpy as np
from plotter import *


isSet4 = 1
PD = '0.6/' if isSet4 else ''
folder = ['./data/','./data_set4/']
folder = folder[isSet4]+PD

if isSet4:
    Data = np.load(folder+"data_set4.npy", allow_pickle=True)
else:
    Data = np.load("data_set2a3.npy", allow_pickle=True)

set2, set3, set4 = 0, 1, 2
SET = ['set2','set3','set4']
navi, pred = 0, 1
N_P = ['navi','pred']

Filename = ["0.Arrival", "1.Collision", "2.Timeout", "3.Congestion", "4.PathEfficiency",
            "5.AveProximity", "6.ClosetDistance", "7.PathIrregularity",
            "8.AveSpeed", "9.AveAccel", "10.AveJerk", "11.AveEnergy"]
YLabel = ["Arrival Rate (%)", "Collision Rate (%)", "Timeout Rate (%)", "Congestion Rate (%)", "Path Efficiency (%)",
    "Average Proximity (m)", "Closet Distance (m)", "Path Irregularity (deg/m)",
    "Average Speed (m/s)", "Average Acceleration (m/s^2)", "Average Jerk (m/s^3)", "Average Energy (m^2/s^2)"]

def skip(set,n_p,m):
    if n_p==pred:
        if m in [0,2]: return True
    return False


''' Plot '''

if isSet4:
    X = [list(range(12)),list(range(9))]
    X_ticks = [ ["w/LM","w/SF","w/ORCA","w/CADRL",  "w/N*LM","w/N*SF","w/N*ORCA","w/N*CADRL",  "w/1*LM","w/1*SF","w/1*ORCA","w/1*CADRL"],
                ["w/CVM","w/SLSTM","w/SPEC",  "w/N*CVM","w/N*SLSTM","w/N*SPEC",  "w/1*CVM","w/1*SLSTM","w/1*SPEC"] ]
    Legend = [ np.array(["LM","SF","ORCA","CADRL"]),
               np.array(["CVM","SLSTM","SPEC"]) ]

    set = set4
    for n_p in [navi,pred]:
        for m in range(12): # metric
            if skip(set,n_p,m): continue
            for y_rescale in [False,True]:
                 fn = SET[set]+'_'+N_P[n_p]+'/'+('r' if y_rescale else '')+Filename[m]
                 plotLineChart( X[n_p], Data[n_p][:,:,m,0],
                         Min=Data[n_p][:,:,m,1], Max=Data[n_p][:,:,m,2],
                         Legend=Legend[n_p],
                         X_ticks=X_ticks[n_p],
                         filename=fn,
                         set=set4,
                         y_rescale=y_rescale,
                         ylabel=YLabel[m],
                         figsize=(8,6) )



else:
    X = [np.arange(5), np.arange(0,1,0.1)+0.1]

    Ag = np.array( [ [[1,2,3,4,0],[5,6,7,0]],
                     [[1,2,3,4],[5,6,7]] ] )
    Legend = np.array(["DATASET","LM","SF","ORCA","CADRL","CVM","SLSTM","SPEC"])
    X_ticks = ['ETH', 'HOTEL', 'UNIV', 'ZARA1', 'ZARA2']

    for set in [set2,set3]:
        for n_p in [navi,pred]:
            for m in range(12): # metric
                if skip(set,n_p,m): continue
                for y_rescale in [False,True]:
                    fn = SET[set]+'_'+N_P[n_p]+'/'+('r' if y_rescale else '')+Filename[m]
                    plotLineChart(X[set],(Data[set][:,Ag[set,n_p],m,0]).T,
                            Min=(Data[set][:,Ag[set,n_p],m,1]).T, Max=(Data[set][:,Ag[set,n_p],m,2]).T,
                            Legend=Legend[Ag[set,n_p]],
                            X_ticks=(X_ticks if set==set2 else None),
                            filename=fn,
                            y_rescale=y_rescale,
                            ylabel=YLabel[m] )
