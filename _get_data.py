import csv
import os
import numpy as np

folder = './data/'


name_map =	{
    "LINEAR": "1_LM",
    "SF": "2_SF",
    "SOCIALFORCE": "2_SF",
    "RVO": "3_ORCA",
    "CADRL": "4_CADRL",
    "CVM": "5_CVM",
    "SLSTM": "6_SLSTM",
    "SPEC": "7_SPEC",
    "realdata":"0_DATASET"
}
PI_coef = {
    "0_DATASET": 0.005,
    "1_LM": 0,
    "2_SF": 0.002,
    "3_ORCA": 0.002,
    "4_CADRL": 0.002,
    "5_CVM": 0,
    "6_SLSTM": 0.002,
    "7_SPEC": 0.002
}
Dts = {"ETH":0, "HOTEL":1, "UNIV":2, "ZARA1":3, "ZARA2":4}
SPD = {"ETH":2.3443115, "HOTEL":1.13897697, "UNIV":0.68300572, "ZARA1":1.12739064, "ZARA2":1.09646748}

ave_path_len = 2.5*1.4
# Col = [7,8,36,28,29,32,33, 12,14,15, 16,18,19, 20,22,23, 24,26,27, 34,35]; output_file="plot.csv"
Col = [7,8,9,43,28,32,33,39,12,16,20,24] #,45,46,31,30,33,36,42,41,14,15,18,19,22,23,26,27];
output_file="output.csv"
ID_PI, ID_V, ID_E, ID_AJ = [39,42,41], [12,14,15], [24,26,27], [16,18,19, 20,22,23]
fl = len(folder)
Data = []
Data_set2 = np.zeros([5,8,12,4])
Data_set3 = np.zeros([10,8,12,4])
Files = []

def exp_id(st):
    if st[1]=='.': id = round( (float(st)-0.1)*10 )
    else: id = Dts[st]
    return id

def rescale(lines,ID=[],k=1,b=0):
    for i in ID:
        for j in range(1,len(lines)):
            lines[j][i] = str(float(lines[j][i])*k+b)
    return lines

def fixCD(lines):
    num = []
    for line in lines[1:-1]:
        num.append( float(line[33]) )
    num = np.array(num)
    lines[-1][33] = num.mean()
    return lines

def getStats(lines,col):
    num = []
    for i in range(1,len(lines)-1):
        num.append( float( lines[i][col] ) )
    num = np.array(num)
    return (num.mean(),num.min(),num.max(),num.std())


for path, dirs, files in os.walk(folder):
    for fn in files:
        # traverse the folder
        if fn[-4:]!=".csv": continue
        path_fn = os.path.join(path,fn)
        if path_fn in Files: continue
        Files.append(path_fn)
        # info from path:
        data = [path_fn]
        if fn!="item_name.csv":
            data += ["set"]
            data += path_fn[fl:].split('/')[:2]
            data[1] = str( int(data[3].split("_")[0][-1])+1 ) #set
            data[2] = name_map[data[2]] #algo
            data[3] = data[3].split("_")[1] #exp
        else:
            data += ["set","algo","exp"]
        # info from CSV:
        csv_file = open(path_fn, "r")
        reader = csv.reader(csv_file)
        lines = list(reader)
        csv_file.close()
        data.append( str(len(lines)-2) ) # "num_iter"
        # correct data:
        if fn!="item_name.csv":
            lines = fixCD(lines)
            lines = rescale(lines,ID_PI,PI_coef[data[2]])
            if data[1]=='3' and (data[2]=="5_CVM" or data[2]=="1_LM"):
                lines = rescale(lines,ID_V,0,1)
                lines = rescale(lines,ID_E,0,1)
                lines = rescale(lines,ID_AJ,0,0)
            if data[1]=='2' and (data[2]=="5_CVM" or data[2]=="1_LM"):
                lines = rescale(lines,ID_V,0,SPD[data[3]])
                lines = rescale(lines,ID_E,0,SPD[data[3]]**2)
                lines = rescale(lines,ID_AJ,0,0)
        # save:
        for i in range(len(Col)):
            data.append(lines[-1][Col[i]])

            if fn!="item_name.csv":
                algo_id = int(data[2][0])
                stats = getStats(lines,Col[i])
                if data[1]=='3':
                    Data_set3[exp_id(data[3]),algo_id,i] = stats
                elif data[1]=='2' or data[1]=='1':
                    Data_set2[exp_id(data[3]),algo_id,i] = stats

        Data.append(data)


csvFile = open(os.path.join("./",output_file), "w")
writer = csv.writer(csvFile)
for line in Data:
    writer.writerow(line)
csvFile.close()

np.save("data_set2a3.npy", [Data_set2,Data_set3])
