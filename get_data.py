import csv
import os
import numpy as np

isSet4 = 1
PD = '0.6/' if isSet4 else ''
folder = ['./data/','./data_set4/']
folder = folder[isSet4]+PD
output_file = folder[:-1]+"_output.csv"

name_map =	{
    "LINEAR": "1_LM",
    "SF": "2_SF",
    "SOCIALFORCE": "2_SF",
    "RVO": "3_ORCA",
    "CADRL": "4_CADRL",
    "CVM": "5_CVM",
    "SLSTM": "6_SLSTM",
    "SPEC": "7_SPEC",
    "realdata":"0_DATASET",
    'NonCooperativePolicy':"1_LM"
}
PI_coef = {
    "0_DATASET": 0.005,
    "1_LM": 0,
    "2_SF": 0.002,
    "3_ORCA": 0.002,
    "4_CADRL": 0.002,
    "5_CVM": 0,
    "6_SLSTM": 0.002,
    "7_SPEC": 0.002
}
Dts = {"ETH":0, "HOTEL":1, "UNIV":2, "ZARA1":3, "ZARA2":4}
SPD = {"ETH":2.3443115, "HOTEL":1.13897697, "UNIV":0.68300572, "ZARA1":1.12739064, "ZARA2":1.09646748}

ave_path_len = 2.5*1.4
# Col = [7,8,36,28,29,32,33, 12,14,15, 16,18,19, 20,22,23, 24,26,27, 34,35]; output_file="plot.csv"
Col = [7,8,9,43,28,32,33,39,12,16,20,24] #,45,46,31,30,33,36,42,41,14,15,18,19,22,23,26,27];
ID_PI, ID_V, ID_E, ID_AJ = [39,42,41], [12,14,15], [24,26,27], [16,18,19, 20,22,23]
fl = len(folder)
Data = []
Data_set2 = np.zeros([5,8,12,4])
Data_set3 = np.zeros([10,8,12,4])
Data_set4 = ( np.zeros([4,9,12,4]), np.zeros([3,6,12,4]) )
Leg_set4 = ( np.empty([4,9], dtype="U20"), np.empty([3,6], dtype="U20") )

Files = []

def exp_id(st):
    if st[1]=='.': id = round( (float(st)-0.1)*10 )
    else: id = Dts[st]
    return id

def rescale(lines,ID=[],k=1,b=0):
    for i in ID:
        for j in range(1,len(lines)):
            lines[j][i] = str(float(lines[j][i])*k+b)
    return lines

def fixCD(lines):
    num = []
    for line in lines[1:-1]:
        num.append( float(line[33]) )
    num = np.array(num)
    lines[-1][33] = num.mean()
    return lines

def getStats(lines,col):
    num = []
    for i in range(1,len(lines)-1):
        num.append( float( lines[i][col] ) )
    num = np.array(num)
    return (num.mean(),num.min(),num.max(),num.std())

def getInfo(set,algo,adv,being1):
    algo_id = int(algo[0])
    n_p = (algo_id>=5)
    if set=="5050":
        leg = algo[2:]+"@5050_w/"+adv[2:]
        id=0
    else:
        leg = algo[2:] + ("@1vN_w/" if being1 else "@Nv1_w/") + adv[2:]
        id = (2-being1)*(3-n_p)
    algo_id = algo_id-(1+n_p*4)
    adv_id = int(adv[0])-(1+n_p*4)
    id += ( adv_id - 1*(adv_id>algo_id) )
    return n_p, algo_id, id, leg


for path, dirs, files in os.walk(folder):
    for fn in files:
        # traverse the folder
        if fn[-4:]!=".csv": continue
        path_fn = os.path.join(path,fn)
        if path_fn in Files: continue
        Files.append(path_fn)
        # info from path:
        data = [path_fn]
        if fn!="item_name.csv":
            if isSet4:
                path_split = path_fn[fl:].split('/')
                set = path_split[1]
                algo = name_map[path_split[4]] #algo

                if set=='5050':
                    algos = path_split[2].split('_')[-3:]
                    algos = [name_map[algos[0]],name_map[algos[2]]]
                else:
                    algos = path_split[2].split('_')[-2:]
                    algos = [name_map[algos[0]],name_map[algos[1]]]

                being1 = (algo==algos[0])
                adv = algos[being1] # adversial algorithm is then the other

                if set=='5050':
                    exp = adv
                else:
                    exp = (algo+'.'+adv) if being1 else (adv+'.'+algo)

            else:
                path_split = path_fn[fl:].split('/')
                setup = path_split[1].split("_")
                set = str( int(setup[0][-1])+1 ) #set
                algo = name_map[path_split[0]] #algo
                exp = setup[1] #exp

            data += [set, algo, exp]

        else:
            data += ["set","algo","exp"]
        # info from CSV:
        csv_file = open(path_fn, "r")
        reader = csv.reader(csv_file)
        lines = list(reader)
        csv_file.close()
        data.append( str(len(lines)-2) ) # "num_iter"
        # correct data:
        if fn!="item_name.csv":
            lines = fixCD(lines)
            lines = rescale(lines,ID_PI,PI_coef[data[2]])
            if data[1]=='3' and (data[2]=="5_CVM" or data[2]=="1_LM"):
                lines = rescale(lines,ID_V,0,1)
                lines = rescale(lines,ID_E,0,1)
                lines = rescale(lines,ID_AJ,0,0)
            if data[1]=='2' and (data[2]=="5_CVM" or data[2]=="1_LM"):
                lines = rescale(lines,ID_V,0,SPD[data[3]])
                lines = rescale(lines,ID_E,0,SPD[data[3]]**2)
                lines = rescale(lines,ID_AJ,0,0)
        # save:
        for i in range(len(Col)):
            data.append(lines[-1][Col[i]])

            if fn!="item_name.csv":
                algo_id = int(data[2][0])
                stats = getStats(lines,Col[i])
                if data[1]=='3':
                    Data_set3[exp_id(data[3]),algo_id,i] = stats
                elif data[1]=='2' or data[1]=='1':
                    Data_set2[exp_id(data[3]),algo_id,i] = stats
                elif data[1]=='5050' or data[1]=='1vsn-1':
                    n_p, algo_id, id, leg = getInfo(set,algo,adv,being1)
                    Leg_set4[n_p][algo_id,id] = leg
                    Data_set4[n_p][algo_id,id,i] = stats

        Data.append(data)


csvFile = open(os.path.join("./",output_file), "w")
writer = csv.writer(csvFile)
for line in Data:
    writer.writerow(line)
csvFile.close()

if isSet4:
    np.save(folder+"data_set4.npy", Data_set4)
else:
    np.save("data_set2a3.npy", [Data_set2,Data_set3])
