import numpy as np
import matplotlib.pyplot as plt

for i in range(10):
    plt.plot([i,i], [0,1], color='C'+str(i), linewidth=6)
    plt.xticks(range(10),range(10))
plt.show()
