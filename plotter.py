import numpy as np
import matplotlib.pyplot as plt

set2, set3, set4 = 0, 1, 2
map = {"DATASET":0, "LM": 1, "SF": 2, "ORCA": 3, "CADRL": 4, "CVM": 5, "SLSTM": 6, "SPEC": 7}
color = ["C0",      "C1",      "C2",    "C5",       "C4",       "C8",     "C9",     "C3"]
def plotLineChart(x,Ave,
        Min=None,Max=None,
        Legend=None,
        filename=None,
        xlabel="Population Density (person/m^2)",
        ylabel="",
        xlim=None,
        ylim=None,
        title=None,
        show=False,
        y_rescale=False,
        X_ticks=None,
        set=None,
        figsize=(6.4,4.8)):
    print(filename)#,'\t\t',ylabel,'\t\t',x.shape,'\t',Ave.shape,'\t',Legend)
    _fig=plt.figure(figsize=figsize)
    N = len(Legend)
    for i in range(N):
        _x = x.copy()
        if set==set4:
            algo_id = map[Legend[i]]
            n_p = (algo_id>=5)
            algo_id = algo_id-(1+n_p*4)
            offset = 4-n_p
            _x.remove(algo_id); _x.remove(algo_id+offset); _x.remove(algo_id+offset*2);
        plt.plot(_x, Ave[i], '.-', label=Legend[i],c=color[algo_id])
        if Min is not None and Max is not None: plt.fill_between(_x, Min[i], Max[i], alpha=0.2,color=color[algo_id])
    plt.legend()
    if title: plt.title(title)
    else: plt.title(ylabel+" - "+ ('Dataset Condition' if set2 else xlabel))
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if xlim: plt.xlim(xlim)
    if ylim: plt.ylim(ylim)
    if y_rescale:
        ymin,ymax=Ave.min(),Ave.max()
        ymargin = (ymax-ymin)*0.05
        plt.ylim([ymin-ymargin,ymax+ymargin])
    if X_ticks is not None: plt.xticks(x, X_ticks,rotation='vertical')
    if filename: plt.savefig("plots/"+filename+".png",dpi=150,bbox_inches = "tight")
    if show: plt.show()
    plt.close(_fig)

def getMinMax(x,Ave,Std=None,coef=1.0):
    x,Ave=np.array(x),np.array(Ave)
    if Std is None:
        Std = []
        for ave in Ave:
            Std.append( [np.var(ave-x*(ave[-1]-ave[0]))]*len(ave) )
    Std = np.array(Std)*coef
    Min = Ave - np.abs(np.random.normal(0,Std))
    Max = Ave + np.abs(np.random.normal(0,Std))
    return (Min,Max)

# if __name__ is "__main__":
#     np.random.seed(0)
#     x = np.arange(0,1,0.1)+0.1
#     Ave = []
#     Ave.append([10,12,11,15,19,20,21,28,33,35]) # fake CADRL
#     Ave.append([11,13,14,19,18,15,20,23,38,40]) # fake RVO
#     Ave.append([6,7,8,13,18,23,20,21,34,30]) # fake social force
#     Ave.append([9,13,12,11,11,15,16,14,19,18]) # fake Navigan
#     Ave.append([20,22,21,25,29,30,31,38,43,45]) # fake linear model
#     Min = Ave - np.random.rand(5,10)*3
#     Max = Ave + np.random.rand(5,10)*3
#     Legend = ['CADRL','RVO','SF','NaviGAN','LM']
#     filename = "collision rate (%)"
#     xlabel = "Population Density (person/m^2)"
#     ylabel = "collision"
#     ylim = [5,60]
#
#     Min,Max=getMinMax(x,Ave)
#     plotLineChart(x,Ave,Min,Max,Legend,
#             filename=filename,
#             xlabel=xlabel,
#             ylabel=ylabel,
#             ylim=ylim,
#             show=True)
